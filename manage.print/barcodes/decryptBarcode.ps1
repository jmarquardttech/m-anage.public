# ------------------------------------------------------------------------
# Script: decryptBarcode.ps1
# Author: Karl Baase - karl.baase@jmarquardt.com
# Date: 29/08/2019
# Comments: https://jmarquardt.atlassian.net/browse/MANAGE-9617 - decrypt m-anage.com lead barcodes
# Copyright: ©2019 - JMarquart Technologies GmbH
# ------------------------------------------------------------------------

# Requieres openssl to be installed locally and being availe on the PATH environment variable
# Barcodes generated via m-anage.com follow the format mMV-I-<visitorId>,<encrypted barcode values>,m
# e.g. mMV-I-33233,jbfMhD6wmkwHABOB4fpezzymVV-YTOGhu4rGHf0ZLaM1,m

# Use the above mentiond <encrypted barcode value> refering to the sample this would be jbfMhD6wmkwHABOB4fpezzymVV-YTOGhu4rGHf0ZLaM1
$global:cryptedMessage = "<encrypted barcode value>"
# JMT can supply this value please contact us - openssl expects HEX representation
$global:AESKey = "<event specific key>"
# JMT can supply this value please contact us - openssl expects HEX representation
$global:AESIV = "<event specific iv>"

# Get the Byte Array via URLTokenDecode
[byte[]] $lUrlTokenDecodedBytes = [System.Web.HttpServerUtility]::UrlTokenDecode($global:cryptedMessage);
# Get the Base64 string for the byte array to pass to openssl
$lBase64EncodedMessage = [System.Convert]::ToBase64String($lUrlTokenDecodedBytes);

Write-Host Converted crypt barcode value to openssl compatible format: $lBase64EncodedMessage

# Call openssl to decrypt
$output = [string] (& echo $lBase64EncodedMessage | openssl enc -d -aes-256-cbc -base64 -K $global:AESKey -iv $global:AESIV  2>&1)
Write-Host Received: $output